
const routes = require('next-routes');

module.exports = routes()
    .add('home','/','index')
    .add('jobs','/jobs','job')
    .add('detail','/jobs/detail/:slug','detail')