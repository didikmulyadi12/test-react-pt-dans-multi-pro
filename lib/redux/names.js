export const SET_CLIENT_STATE   = "SET_CLIENT_STATE";
export const SET_TOKEN          = "SET_TOKEN";
export const SET_DATA           = "SET_DATA";