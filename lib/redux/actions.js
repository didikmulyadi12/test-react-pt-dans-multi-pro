import {
    SET_CLIENT_STATE, SET_TOKEN, SET_DATA
} from './names'

export const setClientState = (state) => ({
    type: SET_CLIENT_STATE,
    payload: state
});

export const setToken = (state) => ({
    type : SET_TOKEN,
    payload : state
})


export const setData = (state) => ({
    type : SET_DATA,
    payload : state
})