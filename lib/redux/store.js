import reducer from "./reducer"
import {createStore} from "redux";
/** 
* @param {object} initialState
* @param {boolean} options.isServer indicates whether it is a server side or client side
* @param {Request} options.req NodeJS Request object (not set when client applies initialState from server)
* @param {Request} options.res NodeJS Request object (not set when client applies initialState from server)
* @param {boolean} options.debug User-defined debug mode param
* @param {string} options.storeKey This key will be used to preserve store in global namespace for safe HMR 
*/


const initialStateRedux = {
    version : '1',
    apps    : 'Apps Skeleton React',
    copyright : '',
    title   : '',
    description : ''
}

const makeConfiguredStore = (reducer, initialState) =>
    createStore(reducer, initialState   );

const makeStore = (initialState, {isServer, req, debug, storeKey}) => {
    let state = {...initialState,...initialStateRedux}

    if (isServer) {
        return makeConfiguredStore(reducer, state);
    } else {

        // we need it only on client side
        const {persistStore, persistReducer} = require('redux-persist');
        const storage = require('redux-persist/lib/storage').default;

        const persistConfig = {
            key: 'OSS-NEXTJS',
            whitelist: ['fromClient','token','dataLogin'], // make sure it does not clash with server keys
            storage
        };

        const persistedReducer = persistReducer(persistConfig, reducer);
        const store = makeConfiguredStore(persistedReducer, state);

        store.__persistor = persistStore(store); // Nasty hack

        return store;
    }
};


export default makeStore