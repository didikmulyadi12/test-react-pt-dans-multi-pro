import {
    SET_CLIENT_STATE, SET_TOKEN, SET_DATA
} from './names'

const reducer = (state, {type, payload}) => {
    switch (type) {
        case 'FOO':
            return {...state, foo: payload};
        case SET_CLIENT_STATE : 
            return { ...state, fromClient: payload };
        case SET_TOKEN :
            return { ...state, token: payload};
        case SET_DATA :
            return { ...state, dataLogin: payload};
        default:
            return state
    }
};

export default reducer;