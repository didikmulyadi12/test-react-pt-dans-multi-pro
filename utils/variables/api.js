export default class Api {

    static baseUrl      =   "https://jobs.github.com/";
    static baseUrlNode  =   "http://localhost:3000/";
    static baseAsset=   "/";

    static header = function(token){
        return {
            headers : {
                'Authorization' : token
            }
        }
    }

    static urlJobList   =   Api.baseUrl+"positions.json";
    static urlDetailJob =   (id) => Api.baseUrl+"positions/"+id+".json";
    static urlLogin     =   Api.baseUrlNode+"api/login";
}