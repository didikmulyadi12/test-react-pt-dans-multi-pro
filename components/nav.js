import React from 'react'
import Head from './main/head'
import {connect} from 'react-redux'
import { setToken } from '../lib/redux/actions';

class Nav extends React.Component {
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div>
        <Head /> 
        <nav className="navbar navbar-expand-md bg-dark navbar-dark">
          <a className="navbar-brand" href="/">Jobs Indonesia</a>
          
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="collapsibleNavbar">
            <ul className="navbar-nav ml-auto">
              {
                this.props.token != "" && this.props.token ? (
                  <div style={{display:'flex'}}>
                    <li className="nav-item">
                      <a className="nav-link">{this.props.dataLogin ? this.props.dataLogin.nama : ""}</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link" onClick={() => this.props.dispatch(setToken(""))} style={{cursor:'pointer'}}>Logout</a>
                    </li>
                  </div>
                ) : (
                  <li className="nav-item">
                    <a className="nav-link" href="/">Login</a>
                  </li>
                )
              }
              
            </ul>
          </div> 
        </nav>
      </div>
    )
  }
}

export default connect(state => state)(Nav);
