// server.js
const next = require('next')
const routes = require('./routes')
const app = next({dev: process.env.NODE_ENV !== 'production'})
const port = process.env.NODE_ENV !== 'production' ? 3000 : 80;
var bodyParser = require('body-parser');
const handler = routes.getRequestHandler(app)

// With express
const express = require('express')


app.prepare().then(() => {

  const server = express()
  server.use(bodyParser.json({ type: 'application/json' }));

  server.get('/api/version',function(req,res){
    res.send({version : '1.00'})
  })
  
  server.post('/api/login',function(req,res){
      const data = {
        nama : 'Didik Mulyadi',
        alamat : 'Menteng',
        ttl : 'Tangerang, 25 Oktober 1997',
        token : '123123123123213asdasda'
      }
      const emailStatic     = "didikmulyadi12@gmail.com";
      const passwordStatic  = "12345678";

      const reqEmail    = req.body.email;
      const reqPassword = req.body.password;

      if(reqEmail.toString() == emailStatic && reqPassword.toString() == passwordStatic){
        res.send({
          message : 'success',
          code : 1,
          status : true,
          data : data
        })
      }else{
        res.send({
          message : 'Email dan password tidak sesuai',
          code : 0,
          status : false,
          data : {}
        })
      }
  })

  server.use(handler).listen(port)
})

