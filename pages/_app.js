import React from "react";
import {Provider} from "react-redux";
import App, {Container} from "next/app";
import withRedux from "next-redux-wrapper";
import makeStore from "../lib/redux/store"
import {PersistGate} from 'redux-persist/integration/react';

class MyApp extends App {

    render(){
        const {Component, pageProps, store} = this.props;
        return (
            <Container>
                <Provider store={store}>
                    <PersistGate persistor={store.__persistor} loading={""}>
                        
                        <Component {...pageProps} />
                    </PersistGate>
                </Provider>
                
            </Container>
        );
    }
}

export default withRedux(makeStore, {debug: true})(MyApp);