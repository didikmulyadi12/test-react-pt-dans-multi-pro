import React from 'react'
import Head from '../components/main/head'
import {connect} from 'react-redux'
import Axios from 'axios';
import Api from '../utils/variables/api';
import swal from 'sweetalert';
import { setToken, setData } from '../lib/redux/actions';

class Index extends React.Component{
  constructor(props){
    super(props)

    this.state ={
      formEmail : '',
      formPassword : ''
    }

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount(){
    if(this.props.token){
      this.props.token != "" ? window.location = "/jobs" : "";
    } 
    console.log(this.props)
  }

  submit = e =>{
    e.preventDefault();

    const data = {
      email : this.state.formEmail,
      password : this.state.formPassword
    };

    Axios.post(Api.urlLogin,data).then(res => {
      if(res.data.message == "success"){
        console.log(res.data.data)
        this.props.dispatch(setToken(res.data.data.token));
        this.props.dispatch(setData(res.data.data));
        window.location = "/jobs";
      }else{
        swal('Gagal!','Email dan password tidak sesuai','error');
      }
    }).catch(err => console.log(err.message));
  }

  onChange = e => {
    let name = e.target.name;
    let value = e.target.value;

    this.setState({
      [name] : value
    })
  }

  render(){
    return (
    <div>
      <Head />
      <div className="boxLogin">
        <form onSubmit={this.submit}>
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input required type="email" className="form-control" id="email" name="formEmail" onChange={this.onChange} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password:</label>
            <input required type="password" className="form-control" id="password" name="formPassword" onChange={this.onChange} />
          </div>
          <div className="form-group">
            <button type="submit" id="login" className="btn btn-success">LOGIN</button>
          </div>
        </form>
      </div>
      <style jsx>{`
        .boxLogin {
          width : 100%;
          height : 240px;
          max-width : 400px;
          padding : 15px;
          box-shadow : 0px 0px 10px 1px rgba(0,0,0,0.2);
          border-radius : 5px;
          margin : auto;
          position :absolute;
          left:0;right:0;bottom:0;top:0;
        }

        #login {
          border :0;
          outline : unset;
          width : 100%;
        }
      `}</style>
    </div>
  )

  }
}

export default connect(state => state)(Index)
