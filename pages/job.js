import React from 'react'
import Axios from 'axios';
import Api from '../utils/variables/api';
import Head from '../components/main/head'
import Nav from '../components/nav'
import {connect} from 'react-redux'

class Job extends React.Component{
    constructor(props){
        super(props)

        this.state ={
            listJob : []
        }
    }

    componentDidMount(){
        Axios.get(Api.urlJobList).then(res => {
            console.log(res);
            this.setState({
                listJob : res.data
            })
        }).catch(err => console.log(err));
    }

    toDetail(id){
        window.location = "/jobs/detail/"+id;
    }

    render(){
        return (
            <div>
                <Nav />
                <div className="container">
                    <div className="job-title">JOB LIST</div>
                    <div id="area-list" className="row">
                        {
                            this.state.listJob.map((value,index) => (
                                <div className="col-12"  key={index}>
                                    <div className="list" onClick={() => this.toDetail(value.id)}>
                                        <div className="area-image">
                                            <img alt="company-logo" src={value.company_logo} width="100px" />
                                        </div>
                                        <div className="area-content">
                                            <div className="company-title">{value.title}</div>
                                            <div className="company-name"><a href={value.company_url}>{value.company}</a></div>
                                            <div className="company-location">
                                                {value.location}
                                            </div>
                                            <div className="position-status">{value.type}</div>
                                        </div>
                                        <div style={{clear:'both'}} />
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
                <style jsx>{`

                    .job-title {
                        font-size : 24px;
                        font-weight:bold;
                        font-family : arial;
                        margin-top : 80px;
                        margin-bottom : 30px;
                    }

                    .area-list {
                        margin-top : 100px;
                    }

                    .list {
                        width : 100%;
                        margin:10px 0px;
                        box-shadow : 0px 0px 3px 1px rgba(0,0,0,0.1);
                        padding : 10px;
                        display : flex;
                        cursor:pointer;
                    }

                    .area-image{
                        margin-right : 25px;
                    }

                    .company-title{
                        font-size : 18px;
                        font-weight:bold;
                        color : #656565;
                        font-family : Arial, Helvetica;
                    }

                    .company-name a {
                        text-decoration : none;
                        font-size : 14px;
                        font-family : Arial, Helvetica;
                        color :#2f7de1;
                    }

                    .company-location {
                        font-size : 14px;
                        color : #AAA;
                        font-family : Arial, Helvetica;
                        margin-bottom : 30px;
                    }

                    .position-status{
                        font-size : 14px;
                        color : #AAA;
                        font-family : Arial, Helvetica;
                    }
                `}</style>
            </div>
        )
    }
}

export default connect(state => state)(Job);