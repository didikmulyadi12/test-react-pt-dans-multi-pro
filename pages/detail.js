import React from 'react'
import Axios from 'axios';
import Api from '../utils/variables/api';
import Head from '../components/main/head'
import {connect} from 'react-redux'

class Detail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            company : {}
        }
    }

    static async getInitialProps({query}) {    
        return { query}
    }

    async componentDidMount(){
        console.log(this.props.query.slug);
        await Axios.get(Api.urlDetailJob(this.props.query.slug)).then(res => {
            this.setState({
                company : res.data
            },() => {
                console.log(this.state.company);
            })
        }).catch(err => console.log(err));
    }

    render(){
        return (
            <div>
                <Head />
                <div className="container ">
                    <div className="job-title" onClick={() => window.location ="/jobs"}>&lt; All Job</div>
                    <div className="list">
                        <div className="area-image">
                            <img alt="company-logo" src={this.state.company.company_logo} width="100px" />
                        </div>
                        <div className="area-content">
                            <div className="company-title">{this.state.company.title}</div>
                            <div className="company-name"><a href={this.state.company.company_url}>{this.state.company.company}</a></div>
                            <div className="company-location">
                                {this.state.company.location}
                            </div>
                            <div className="position-status">{this.state.company.type}</div>
                        </div>
                        <div style={{clear:'both'}} />
                    </div>
                    <div className="labelCustom">Job Description</div>
                    <div className="content" dangerouslySetInnerHTML={{__html:this.state.company.description}} />
                    <div className="labelCustom">How To Apply</div>
                    <div className="content" dangerouslySetInnerHTML={{__html:this.state.company.how_to_apply}} />
                </div>
                <style jsx>{`

                    .job-title {
                        font-size : 24px;
                        font-weight:bold;
                        font-family : arial;
                        margin-top : 80px;
                        cursor:pointer;
                    }

                    .labelCustom{
                        font-size : 16px;
                        font-weight:bold;
                        color : #656565;
                        font-family : Arial, Helvetica;
                        margin-top :30px;
                    }

                    .content {
                        box-shadow : 0px 0px 3px 1px rgba(0,0,0,0.1);
                        border-radius : 5px;
                        padding : 15px;
                        margin-top : 15px;
                    }

                    .area-list {
                        margin-top : 100px;
                    }

                    .list {
                        width : 100%;
                        margin:50px 0px 20px 0px;
                        box-shadow : 0px 0px 3px 1px rgba(0,0,0,0.1);
                        padding : 10px;
                        display : flex;
                        border-radius : 5px;
                        cursor:pointer;
                    }

                    .area-image{
                        margin-right : 25px;
                    }

                    .company-title{
                        font-size : 20px;
                        font-weight:bold;
                        color : #656565;
                        font-family : Arial, Helvetica;
                    }

                    .company-name a {
                        text-decoration : none;
                        font-size : 14px;
                        font-family : Arial, Helvetica;
                        color :#2f7de1;
                    }

                    .company-location {
                        font-size : 14px;
                        color : #AAA;
                        font-family : Arial, Helvetica;
                        margin-bottom : 30px;
                    }

                    .position-status{
                        font-size : 14px;
                        color : #AAA;
                        font-family : Arial, Helvetica;
                    }
                `}</style>
            </div>
        )
    }
    
}

export default connect(state => state)(Detail);